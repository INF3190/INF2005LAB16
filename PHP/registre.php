<?php
require 'head.php';

$etudiants = array(
    array("CHAJ12345678","Charles","Jordan"),
    array("MARD22345654","Marc","Daniel"),
    array("BENM22347254","Ben Idriss","Martine"),
    array("GAGD11345657","Gagnon","Daniella"),
    array("SORY15345633","Soro","Yaya")
);

$cours = array (
    "INF1120"=>"Programmation 1",
    "INF2005"=>"Programmation web",
    "INF3005"=>"Programmation web avancée"    
);

$inscriptions = array(
    "INF1120" => array("CHAJ12345678","BENM22347254"),
    "INF2005" => array("GAGD11345657","SORY15345633","BENM22347254"),
    "INF3005" => array()
);

echo "<pre>";
print_r($etudiants);
echo "</pre>";
echo "<pre>";
echo json_encode($cours,JSON_UNESCAPED_UNICODE);
echo "</pre>";
//affiche la liste des étudiants dans une table
// @etuds un a tableau contenant des tableau de 3 elements
// codePermanent, nom, prénom sans clés associatives
function tabListeEtudiants($etuds=array()){
    ?>
    <table class="table table-striped">
    <caption class="h3">Liste des étudiants</caption>
  <tr>
    <th>Code Permanent</th>
    <th>Nom</th>
    <th>Prénom</th>
  </tr>
  <?php 
  foreach ($etuds as $etud)
  {
      $codePermanent = $etud[0];
      $nom = $etud[1];
      $prenom = $etud[2];
  ?>
  <tr>
    <td><?=$codePermanent ?></td>
    <td><?=$nom ?></td>
    <td><?=$prenom ?></td>
  </tr>
  <?php 
  }// Fermeture foreach. 
  ?>
</table>

    <?php 
} // fermeture fonction

// retourne un jon des cours auxquels on est inscrit
function mesCours($codeP=""){
    global $inscriptions;
    $returnval=array();
    if(!empty($codeP)){
      foreach ($inscriptions as $cours=>$inscrits){
          if(in_array($codeP,$inscrits)){
              $cle='cours';
              $returnval[]=array($cle=>$cours);
          }
      }
    }
    return json_encode($returnval,JSON_UNESCAPED_UNICODE);
}

tabListeEtudiants($etudiants);
$codePerm="BENM22347254";
$mesCo= mesCours($codePerm);
echo "<h3>Cours auxquels $codePerm est inscrit</h3>";
echo "<pre>";
echo $mesCo;
echo "</pre>";
require 'tail.php';